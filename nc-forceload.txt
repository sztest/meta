------------------------------------------------------------------------

- Want some mechanism to keep blocks/areas force-loaded
- Some kind of thing that loads mapblock it's in, possibly up to N neighbors
  - Maybe neighbors based on distance
- Some kind of machine that needs to be "fed"
  - Works based on alternating optic signals?  Faster pulsers increase effect?
  - New node type?  Recipe?
- Load balancing
  - Track forceload areas and priorities in mod storage
  - Random lottery each cycle for each area
    - ...or each block within each area, so blocks farther from center have less effect
  - Configurable custom limit, fallback on system limit
  
  ------------------------------------------------------------------------
